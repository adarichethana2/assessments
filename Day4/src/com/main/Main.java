package com.main;

 

import com.model.Circle;
import com.model.Square;
import com.model.Rectangle;
import java.util.Scanner;

 

public class Main {

 

    public static void main(String[] args) {
        Circle circle1 = new Circle("circle", 25);
        Square square1 = new Square("square", 4);
        Rectangle rectangle1 = new Rectangle("rectangle", 5, 6);

 


        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the shape name");
        String name = scanner.nextLine();

 

        if (name.equalsIgnoreCase("Circle")) {
            System.out.println("enter the radius");
            int radius=scanner.nextInt();
            circle1.setRadius(radius);
            circle1.getRadius();
            // int radius = scanner.nextInt();
            System.out.println("The area of Circle is :"+circle1.calculateArea());
            System.exit(0);
        }
        if (name.equalsIgnoreCase("Square")) {
            System.out.println("enter the side");
            int side = scanner.nextInt();
            square1.setSide(side);
            square1.getSide();
            // int side = scanner.nextInt();
            System.out.println("The area of square is :" + square1.calculateArea());
            System.exit(0);
        }
        if (name.equalsIgnoreCase("Rectangle")) {
            System.out.println("enter the breadth");
            
            int breadth = scanner.nextInt();
            rectangle1.setBreadth(breadth);
            rectangle1.getBreadth();
            System.out.println("enter the length");
            
             int length = scanner.nextInt();
             rectangle1.setLength(length);
             rectangle1.getLength();
            System.out.println( "The area of rectangle is :"+ rectangle1.calculateArea());
            System.exit(0);
        }
        else {
            System.out.println("It is not correct shape");
        }
        scanner.close();
    }

 

}
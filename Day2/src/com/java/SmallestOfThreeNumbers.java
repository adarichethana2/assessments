package com.java;

import java.util.Scanner;

public class SmallestOfThreeNumbers {

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the first number\n");
		int num1 = sc.nextInt();
		System.out.print("Enter the second number\n");
		int num2 = sc.nextInt();;
		
		System.out.print("Enter the Third number\n");
		int num3 = sc.nextInt();;
		

		findLargest(num1, num2, num3);
	}

	static void findLargest(int num1, int num2, int num3) {
		if (num1 <= num2 && num1 <= num3) {
			System.out.println(num1 + " is the smallest number");

		} else if (num2 <= num1 && num2 <= num3) {
			System.out.println(num2 + " is the smallest number");

		} else {
			System.out.println(num3 + " is the smallest number");

		}

	}
}
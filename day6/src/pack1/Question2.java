
//In the previous program remove the try{}catch(){} block surrounding the sleep method
/*and try to execute the code.
What is your observation? //

IT SHOWS COMPILER ERROR:

The method Thread.sleep throws InterruptedException
If it detects that the current thread has its interrupt flag set,
waking up from its sleep early and allowing you to use the exception
to relocate control to somewhere outside the current flow.
That flag gets set only if something calls interrupt on the thread.

Since our program doesn't call interrupt on any of the threads, 
InterruptedException will not get thrown when you run this program.
The compiler still requires you to catch the exception 
since it is a checked exception declared on the sleep method.
*/
package pack1;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Question2 {

	public static void main(String[] args) throws InterruptedException {
		Question1 t1 = new Question1();

		System.out.println("Name of t1:" + t1.getName());

		t1.start();

		t1.setName("MyThread");
		System.out.println("After changing name of t1:" + t1.getName());
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("kk:mm:ss");
		System.out.println("time : " + sdf.format(date));

		Thread.sleep(10000);
		Date date1 = new Date();
		SimpleDateFormat sdf1 = new SimpleDateFormat("kk:mm:ss");
		System.out.println("time after sleep : " + sdf1.format(date1));

	}

}




package demo;

import java.util.*;

public class ExceptionTest {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		try {
			System.out.println("enter the number : ");
			int a = new Integer(sc.nextInt());
			System.out.println("enter the number : ");
			int b = new Integer(sc.nextInt());
			try {

				System.out.println("" + (a / b));
			} catch (InputMismatchException e) {
				System.out.println("java.util.InputMismatchException");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		sc.close();

	}
}